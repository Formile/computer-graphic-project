#include "BasicApp.h"



BasicApp::BasicApp()
    :addingMode(false),
    CursorNode(nullptr),
    mShutdown(false),
    mRoot(nullptr),
    mCamera(nullptr),
    mSceneMgr(nullptr),
    mWindow(nullptr),
    mResourcesCfg(""),
    mPluginsCfg(""),
    mCameraMan(nullptr),
    mRenderer(nullptr),
    mMouse(nullptr),
    mKeyboard(nullptr),
    mInputMgr(nullptr),
    mLMouseDown(false),
    movingMode(false),
    mRMouseDown(false),
    myGUI(nullptr),
    mRayScnQuery(nullptr),
    mMovableFound(false),
    rotationMode(false),
    scalingMode(false)
{
}

BasicApp::~BasicApp()
{
    for(auto pointer: parts)
        delete pointer;

    if (mCameraMan) delete mCameraMan;

    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);

    delete mRoot;

}

void BasicApp::go()
{
  #ifdef _DEBUG
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
  #else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
  #endif

  if (!setup())
    return;

  mRoot->startRendering();

  destroyScene();
}


bool BasicApp::has_intersections(Ogre::Entity* ent)
{
    Ogre::AxisAlignedBox my_box = ent->getWorldBoundingBox(true);
    //std::cerr << my_box << "vs\n";
    for(LogicBody* part: parts)
    {
        Ogre::Entity* cur_ent = part->ent;
        if( cur_ent != ent)
        {
            Ogre::AxisAlignedBox cur_box = cur_ent->getWorldBoundingBox(true);
            //std::cerr << cur_box <<"\n\n";

            //std::cerr << cur_box.getCenter() <<"\n\n";
            Ogre::AxisAlignedBox intersect = my_box.intersection(cur_box);
            //std::cerr << intersect << "\n\n";

            if(!intersect.isNull())
            {
                std::cerr << "intersection found with " << cur_ent->getMesh()->getName() << "\n";
                return true;
            }
        }
    }
    return false;
}

bool BasicApp::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
    CEGUI::System::getSingleton().injectTimePulse(fe.timeSinceLastFrame);
    if (mKeyboard->isKeyDown(OIS::KC_ESCAPE))
        mShutdown = true;

    if (mShutdown)
    {
        save_scene("backup");
        return false;
    }

    if (mWindow->isClosed())
        return false;


    mKeyboard->capture();
    mMouse->capture();

    mCameraMan->frameRenderingQueued(fe);
    Ogre::Vector3 cam_pos = mCamera->getDerivedPosition();
    if(cam_pos.y <= 10)
    {
        cam_pos.y = 10;
        mCamera->setPosition(cam_pos);
    }

    int dist = 3000;

    if(cam_pos.squaredLength() > sqr(dist))
    {
        //raycast to bounding sphere
        cam_pos.normalise();
        cam_pos *= dist;
        mCamera->setPosition(cam_pos);
    }

    for(LogicBody* cur_part : parts)
        cur_part->chanage_state(fe);

    if(myGUI->openingMode)
    {
        load_scene(myGUI->filename);
        myGUI->openingMode = false;
    }
    else if(myGUI->savingMode)
    {
        save_scene(myGUI->filename);
        myGUI->savingMode = false;
    }

    if(!parts.empty())
        myGUI->setPart((*cur_part_it));

    if(!(myGUI->added_parts.empty()) && !addingMode)
    {
        if(!parts.empty())
            CursorNode->showBoundingBox(false);

        addingMode = true;
        std::string mesh_name = myGUI->added_parts.front();
        std::cerr << "creating " << mesh_name;
        myGUI->added_parts.pop();

        std::cerr << "Creating of new ent\n";
        CursorEnt = mSceneMgr->createEntity(mesh_name);
        CursorEnt->setVisible(false);
        CursorNode = mSceneMgr->getSceneNode("Cursor");
        CursorNode->attachObject(CursorEnt);
        CursorNode->showBoundingBox(true);
    }


  return true;
}

bool BasicApp::keyPressed(const OIS::KeyEvent& ke)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectKeyDown((CEGUI::Key::Scan)ke.key);
    context.injectChar((CEGUI::Key::Scan)ke.text);
    CEGUI::Vector2f mousePos = context.getMouseCursor().getPosition();

    if(parts.empty())
        return true;

    if(mousePos.d_x > mWindow->getWidth()/2)
    {
        if(ke.key == OIS::KeyCode::KC_DELETE)
        {
            CursorNode->detachAllObjects();
            mSceneMgr->destroyEntity(CursorEnt);
            mSceneMgr->destroySceneNode(CursorNode);
            parts.erase(cur_part_it);
            cur_part_it = parts.begin();
            if(!parts.empty())
            {
                myGUI->setPart(*cur_part_it);
                CursorNode = (*cur_part_it)->node;
                CursorEnt = (*cur_part_it)->ent;
                CursorNode->showBoundingBox(true);
            }
        }
        if(ke.key == OIS::KeyCode::KC_S)
        {
            scalingMode = true;
            //CursorNode->showBoundingBox(false);
            CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
            CEGUI::Vector2f mousePos = context.getMouseCursor().getPosition();
            int dx = mousePos.d_x - 3 * mWindow->getWidth() / 4;
            int dy = mousePos.d_y - mWindow->getHeight()/2;
            scalePrevDist = abs(dx) + abs(dy);
            scalePrevVal = (*cur_part_it)->scale;
            //CursorNode->showBoundingBox(true);
        }

        if(ke.key == OIS::KeyCode::KC_T)
        {
            movingMode = true;
            //CursorNode->showBoundingBox(false);

        }
        if(ke.key == OIS::KeyCode::KC_R)
        {
            rotationMode = true;
            //CursorNode->showBoundingBox(false);
        }
  }
    return true;
}

bool BasicApp::keyReleased(const OIS::KeyEvent& ke)
{
    return true;
}

bool BasicApp::mouseMoved(const OIS::MouseEvent& me)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseMove(me.state.X.rel, me.state.Y.rel);
    CEGUI::Vector2f mousePos = context.getMouseCursor().getPosition();
    if( mousePos.d_x * 2 > me.state.width && (addingMode || movingMode))
    {
        Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(
                      2 * (mousePos.d_x - me.state.width/2) / float(me.state.width),
                      mousePos.d_y / float(me.state.height));

        std::pair<bool, Ogre::Real> point = mouseRay.intersects(plane);
        if( point.first )
        {
            Ogre::Vector3 pos = mouseRay.getPoint(point.second);
            if(fabs(pos.x) <= 750 && fabs(pos.z) <= 750)
            {
                if(movingMode)
                {
                    CursorEnt->setVisible(!has_intersections(CursorEnt));
                    (*cur_part_it)->pos.x = pos.x;
                    (*cur_part_it)->pos.z = pos.z;
                    (*cur_part_it)->isDirty = true;
                }
                else
                    if(addingMode)
                    {
                        CursorNode->setPosition(pos);
                        CursorEnt->setVisible(!has_intersections(CursorEnt));
                    }
            }
        }

    }
    else if(rotationMode)
    {
        int cur_rotate = (int((*cur_part_it)->rotation_yaw) + me.state.X.rel + 180 + 360) % 360 - 180;
        (*cur_part_it)->rotation_yaw = cur_rotate;
        (*cur_part_it)->isDirty = true;
        myGUI->setPart((*cur_part_it)); //sync
        CursorEnt->setVisible(!has_intersections(CursorEnt));

    }
    else if(scalingMode)
    {
        int dx = mousePos.d_x - 3 * mWindow->getWidth() / 4;
        int dy = mousePos.d_y - mWindow->getHeight()/2;
        double dist = abs(dx) + abs(dy);
        double ratio = dist/scalePrevDist;
        (*cur_part_it)->scale = scalePrevVal * ratio;
        (*cur_part_it)->isDirty = true;
        myGUI->setPart(*cur_part_it); //sync
        CursorEnt->setVisible(!has_intersections(CursorEnt));

    }

    mCameraMan->injectPointerMove(me);

  return true;
}

// Helper function for mouse events
CEGUI::MouseButton convertButton(OIS::MouseButtonID id)
{
  switch (id)
  {
  case OIS::MB_Left:
    return CEGUI::LeftButton;
  case OIS::MB_Right:
    return CEGUI::RightButton;
  case OIS::MB_Middle:
    return CEGUI::MiddleButton;
  default:
    return CEGUI::LeftButton;
  }
}

bool BasicApp::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonDown(convertButton(id));


    if (id == OIS::MB_Left)
    {
        CEGUI::Vector2f mousePos = context.getMouseCursor().getPosition();
        if( mousePos.d_x * 2 > me.state.width)
        {
            mCameraMan->injectPointerDown(me, id);
            if(addingMode || rotationMode || scalingMode || movingMode)
            {
                if(!CursorEnt->isVisible())
                    return true;

                if(addingMode)
                {
                    std::cerr << "Creating of new node\n";
                    Ogre::SceneNode* nod = mSceneMgr->getRootSceneNode()->createChildSceneNode();
                    CursorNode->detachAllObjects();

                    std::cerr << "attaching\n";
                    nod->attachObject(CursorEnt);
                    std::cerr << "pushing\n";
                    if(CursorEnt->getMesh()->getName() == "robot.mesh")
                    {
                        Ogre::ManualObject* manual = mSceneMgr->createManualObject();
                        mSceneMgr->getRootSceneNode()->attachObject(manual);
                        MovingObject* mov = new MovingObject(nod, CursorEnt, CursorNode->getPosition(), manual);

                        std::vector<Ogre::Vector2> path = analyse_points(CursorNode->getPosition(), CursorEnt->getBoundingRadius());
                        for(auto point: path)
                        {
                            mov->add_point(Ogre::Vector3(point.x, 0, point.y));
                        }

                        parts.push_back(mov);
                    }
                    else
                    {
                        parts.push_back(new LogicBody(nod, CursorEnt));
                        parts.at(parts.size()-1)->pos = CursorNode->getPosition();
                        parts.at(parts.size()-1)->isDirty = true;
                    }

                    cur_part_it = parts.end()-1;
                    myGUI->setPart(parts.at(parts.size()-1));
                    CursorNode = nod;
                    // CusorEnt has not changed
                    CursorNode->showBoundingBox(true);
                    addingMode = false;

                }
                else if(movingMode)
                {
                    CursorNode->showBoundingBox(true);
                    movingMode = false;
                }
                else if(rotationMode)
                {
                    CursorNode->showBoundingBox(true);
                    rotationMode = false;
                }
                else if(scalingMode)
                {
                    CursorNode->showBoundingBox(true);
                    scalingMode = false;
                }
            }
           else
            {
                if(parts.empty())
                    return true;

                CursorNode->showBoundingBox(false);
                Ogre::Ray mouseRay =
                  mCamera->getCameraToViewportRay(
                    2* (mousePos.d_x - me.state.width/2) / float(me.state.width),
                    mousePos.d_y / float(me.state.height));

                mRayScnQuery = mSceneMgr->createRayQuery(Ogre::Ray());
                mRayScnQuery->setRay(mouseRay);
                mRayScnQuery->setSortByDistance(true);

                Ogre::RaySceneQueryResult& result = mRayScnQuery->execute();
                Ogre::RaySceneQueryResult::iterator it = result.begin();
                for ( ; it != result.end(); it++)
                {
                  mMovableFound =
                    it->movable &&
                    it->movable->getName() != "" &&
                    it->movable->getName() != "PlayerCam" &&
                    it->movable->getName() != "floor";

                  if (mMovableFound)
                  {
                    CursorNode = it->movable->getParentSceneNode();
                    for(auto it = parts.begin(); it != parts.end(); ++it)
                        if( (*it)->node == CursorNode)
                        {
                            cur_part_it = it;
                            CursorEnt = (*it)->ent;
                            myGUI->setPart(*it);
                            break;
                        }
                    break;
                  }
                }
                mSceneMgr->destroyQuery(mRayScnQuery);
                CursorNode->showBoundingBox(true);
                mMovableFound = false;
            }
        }
    }

    return true;
}

bool BasicApp::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonUp(convertButton(id));
    mCameraMan->injectPointerUp(me, id);
    return true;
}

void BasicApp::windowResized(Ogre::RenderWindow* rw)
{
  unsigned int width, height, depth;
  int left, top;
  rw->getMetrics(width, height, depth, left, top);

  const OIS::MouseState& ms = mMouse->getMouseState();
  ms.width = width;
  ms.height = height;
}

void BasicApp::windowClosed(Ogre::RenderWindow* rw)
{
  if (rw == mWindow)
  {
    if (mInputMgr)
    {
      mInputMgr->destroyInputObject(mMouse);
      mInputMgr->destroyInputObject(mKeyboard);

      OIS::InputManager::destroyInputSystem(mInputMgr);
      mInputMgr = 0;
    }
  }
}


bool BasicApp::setup()
{
  mRoot = new Ogre::Root(mPluginsCfg);

  setupResources();

  if (!configure())
    return false;

  chooseSceneManager();
  createCamera();
  createViewports();

  Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

  createResourceListener();
  loadResources();

  setupCEGUI();

  createScene();

  createFrameListener();

  return true;
}

bool BasicApp::configure()
{
  if (!mRoot->showConfigDialog())
  {
    return false;
  }

  mWindow = mRoot->initialise(true, "ITutorial");

  return true;
}

void BasicApp::chooseSceneManager()
{
  mSceneMgr = mRoot->createSceneManager(Ogre::ST_EXTERIOR_CLOSE);
}

void BasicApp::createCamera()
{
  mCamera = mSceneMgr->createCamera("PlayerCam");
  mCameraMan = new OgreBites::SdkCameraMan(mCamera);
  mCameraMan->setStyle(OgreBites::CameraStyle::CS_ORBIT);
}

void BasicApp::createScene()
{

    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    mSceneMgr->setSkyDome(true, "Examples/CloudySky", 5, 8);

    mCamera->setNearClipDistance(0.1);
    mCamera->setFarClipDistance(50000);

    Ogre::Vector3 lightDir(0.55, 0.3, 0.75);
    lightDir.normalise();

    Ogre::Light* light = mSceneMgr->createLight("SceneLight");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(lightDir);
    light->setDiffuseColour(Ogre::ColourValue(0.4, 0.4, 0.4));
    light->setSpecularColour(Ogre::ColourValue(0.2, 0.2, 0.2));

    plane = Ogre::Plane(Ogre::Vector3::UNIT_Y, 0);
    Ogre::MeshManager::getSingleton().createPlane(
      "ground",
      Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
      plane,
      1500, 1500, 20, 20,
      true,
      1, 5, 5,
      Ogre::Vector3::UNIT_Z);
    Ogre::Entity* groundEntity = mSceneMgr->createEntity("floor","ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(groundEntity);
    groundEntity->setCastShadows(false);
    groundEntity->setMaterialName("Examples/Rockwall");

    Ogre::ParticleSystem* sunParticle = mSceneMgr->createParticleSystem("Sun", "Examples/PurpleFountain");
    Ogre::SceneNode* particleNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Particle");
    particleNode->attachObject(sunParticle);
    particleNode->translate(Ogre::Vector3(100,0,100));


    main_nod = mSceneMgr->getRootSceneNode()->createChildSceneNode();
    main_nod->setPosition(Ogre::Vector3(0, 50, 0));
    mCameraMan->setTarget(main_nod);


    myGUI = new ScrollController(this);
    CursorNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("Cursor");
    load_scene("backup");

}


void BasicApp::destroyScene()
{
}

void BasicApp::createFrameListener()
{
  Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");

  OIS::ParamList pl;
  size_t windowHnd = 0;
  std::ostringstream windowHndStr;

  mWindow->getCustomAttribute("WINDOW", &windowHnd);
  windowHndStr << windowHnd;
  pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

  mInputMgr = OIS::InputManager::createInputSystem(pl);

  mKeyboard = static_cast<OIS::Keyboard*>(
    mInputMgr->createInputObject(OIS::OISKeyboard, true));
  mMouse = static_cast<OIS::Mouse*>(
    mInputMgr->createInputObject(OIS::OISMouse, true));

  mKeyboard->setEventCallback(this);
  mMouse->setEventCallback(this);

  windowResized(mWindow);

  Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

  mRoot->addFrameListener(this);

  Ogre::LogManager::getSingletonPtr()->logMessage("Finished");
}

void BasicApp::createViewports()
{
  Ogre::Viewport* vp = mWindow->addViewport(mCamera,1, 0.5,0, 0.5,1);

    vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

  mCamera->setAspectRatio(
    Ogre::Real(vp->getActualWidth()) /
    Ogre::Real(vp->getActualHeight()));
}

void BasicApp::setupResources()
{
  Ogre::ConfigFile cf;
  cf.load(mResourcesCfg);

  Ogre::String secName, typeName, archName;
  Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();

  while (secIt.hasMoreElements())
  {
    secName = secIt.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator setIt;

    for (setIt = settings->begin(); setIt != settings->end(); ++setIt)
    {
      typeName = setIt->first;
      archName = setIt->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
    archName, typeName, secName);
    }
  }
}

void BasicApp::createResourceListener()
{
}

void BasicApp::loadResources()
{
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

bool BasicApp::setupCEGUI()
{
  Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing CEGUI ***");

  mRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");

  CEGUI::ImageManager::getSingletonPtr()->loadImageset("../media/images/samples.imageset");
  CEGUI::ImageManager::getSingletonPtr()->loadImageset("samples.imageset");


  CEGUI::SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("WindowsLook.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("VanillaSkin.scheme");

  CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-10.font");

  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

  context.setDefaultFont("DejaVuSans-10");
  context.getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");
  Ogre::LogManager::getSingletonPtr()->logMessage("Finished");

  return true;
}

void BasicApp::load_scene(std::string file_name)
{
    using namespace rapidxml;
    for(LogicBody* p: parts)
    {
        p->node->detachAllObjects();
        mSceneMgr->destroyEntity(p->ent);
        mSceneMgr->destroySceneNode(p->node);
    }
    parts.clear();

    std::ifstream file(file_name + ".scene");
    std::stringstream buffer;
    buffer << file.rdbuf();
    std::string content(buffer.str());

    xml_document<> doc;
    doc.parse<0>(&content[0]);
    xml_node<> *nodes = doc.first_node("scene")->first_node("nodes");
    for( xml_node<> *node = nodes->first_node("node"); node != 0; node = node->next_sibling())
    {
        std::cerr <<"Initiating\n";
        std::string meshName = node->first_node("entity")->first_attribute("meshFile")->value();
        std::string nodeName = node->first_attribute("name")->value();

        std::cerr << "Adding entity " << meshName << "\n";
        Ogre::Entity* ent = mSceneMgr->createEntity(meshName);

        std::cerr << "Adding node " << nodeName << "\n";
        Ogre::SceneNode* ogre_node = mSceneMgr->getRootSceneNode()->createChildSceneNode();

        std::cerr << "Adding attaching and pushing" << nodeName << "\n";
        ogre_node->attachObject(ent);
        parts.push_back(new LogicBody(ogre_node, ent));

        std::string buf = node->first_node("scale")->first_attribute("x")->value();
        parts.at(parts.size()-1)->scale = std::stof(buf);

        buf = node->first_node("rotation")->first_attribute("qy")->value();
        parts.at(parts.size()-1)->rotation_yaw = std::stoi(buf);

        xml_node<> *position = node->first_node("position");
        buf = position->first_attribute("x")->value();
        parts.at(parts.size()-1)->pos.x = std::stoi(buf);
        buf = position->first_attribute("y")->value();
        parts.at(parts.size()-1)->pos.y = std::stoi(buf);
        buf = position->first_attribute("z")->value();
        parts.at(parts.size()-1)->pos.z = std::stoi(buf);
        parts.at(parts.size()-1)->isDirty = true;
        std::cerr << "finished\n";
    }

    if(!parts.empty())
    {
        cur_part_it = parts.begin();
        myGUI->setPart(*cur_part_it);

        CursorNode = (*cur_part_it)->node;
        CursorEnt = (*cur_part_it)->ent;
        CursorNode->showBoundingBox(true);
    }
}

double get_dist(pdd pa, pdd pb)
{
    return sqrt(sqr(pa.first - pb.first) + sqr(pa.second - pb.second));
}

std::map< pdd, std::vector< pdd > > BasicApp::convert_points_to_graph(std::vector<Ogre::Vector2> const& points,std::vector<Ogre::Vector2> const& holes, double min_dist)
{
    start_id = 0;
    finish_id = 1;
    start_point = std::make_pair(points[start_id].x, points[start_id].y);
    finish_point = std::make_pair(points[finish_id].x, points[finish_id].y);

    triangulateio* input = new triangulateio;

    input->numberofpoints = points.size();
    input->numberofsegments = points.size() - 2;
    input->numberofholes = holes.size();
    input->numberofregions = 0;
    input->numberofpointattributes = 0;
    input->numberoftriangleattributes = 0;
    input->numberoftriangles = 0;
    input->numberofcorners = 0;
    input->numberofedges = 0;

    input->pointlist = new REAL[points.size() * 2];
    input->segmentlist = new int[points.size() * 2 - 4];
    input->holelist = new REAL[holes.size() * 2];
    input->pointmarkerlist = NULL;
    input->segmentmarkerlist = NULL;

    int i = 0;
    for(Ogre::Vector2 const& point: points)
    {
        input->pointlist[i] = point.x;
        input->pointlist[i+1] = point.y;
        i += 2;
    }

    i = 0;
    for(Ogre::Vector2 const& hole: holes)
    {
        input->holelist[i] = hole.x;
        input->holelist[i+1] = hole.y;
        i += 2;
    }

    int pos = 0;
    for(int i = 2; i + 3 < points.size(); i +=4)
    {
        input->segmentlist[pos++] = i;
        input->segmentlist[pos++] = i+1;

        input->segmentlist[pos++] = i+1;
        input->segmentlist[pos++] = i+2;

        input->segmentlist[pos++] = i+2;
        input->segmentlist[pos++] = i+3;

        input->segmentlist[pos++] = i+3;
        input->segmentlist[pos++] = i;
    }
    std::cerr << "triangulation" << std::endl;
    triangulateio* output = new triangulateio;
    output->pointlist = NULL;
    output->pointattributelist = NULL;
    output->pointmarkerlist = NULL;
    output->trianglelist = NULL;
    output->neighborlist = NULL;
    output->segmentlist = NULL;
    output->segmentmarkerlist = NULL;
    output->edgelist = NULL;
    output->edgemarkerlist = NULL;
    triangulate("zpq0D",input, output, NULL);
    std::cerr << "triangulation finished" << std::endl;

    std::cerr << "number of triangles is" << output->numberoftriangles << std::endl;
    std::cerr << "number of corners is" << output->numberofcorners << std::endl;

    std::vector <std::pair<double, double> > new_points;
    for(i=0; i < output->numberofpoints * 2; i += 2)
    {
        new_points.emplace_back(output->pointlist[i], output->pointlist[i+1]);
    }
    std::cerr << "sizeof new_points " << new_points.size() << std::endl;

    std::map<std::pair<double, double>, std::vector<std::pair<double, double> >> graph;
    for(i=0; i < output->numberoftriangles * 3; i += 3)
    {
        std::vector <std::pair<int, int> > tuples;
        tuples.push_back(std::make_pair(output->trianglelist[i], output->trianglelist[i+1]));
        tuples.push_back(std::make_pair(output->trianglelist[i+1], output->trianglelist[i+2]));
        tuples.push_back(std::make_pair(output->trianglelist[i+2], output->trianglelist[i]));
        std::vector<std::pair<double, double>> connected_points;
        std::vector<std::pair<std::pair<double, double>, std::pair<double, double>> > special_edges;

        bool finish_point_found = (output->trianglelist[i] == finish_id || output->trianglelist[i+1] == finish_id || output->trianglelist[i+2] == finish_id);
        bool start_point_found = (output->trianglelist[i] == start_id || output->trianglelist[i+1] == start_id || output->trianglelist[i+2] == start_id);

        for(auto tup:tuples)
        {

            pdd point_a = new_points[tup.first];
            pdd point_b = new_points[tup.second];
            pdd mid_point = std::make_pair((point_a.first + point_b.first) / 2, (point_a.second + point_b.second) / 2);
            std::cerr << get_dist(new_points[tup.first], new_points[tup.first])  <<" vs "  << min_dist << std::endl;
            if( get_dist(new_points[tup.first], new_points[tup.second]) >= min_dist)
            {
                connected_points.push_back(mid_point);
            }
            if( start_id == tup.first || start_id == tup.second)
            {
                special_edges.push_back(std::make_pair(new_points[start_id], mid_point));
            }
            else if( finish_id == tup.first || finish_id == tup.second)
            {
                special_edges.push_back(std::make_pair(new_points[finish_id], mid_point));
            }
            else if(start_point_found)
            {
                special_edges.push_back(std::make_pair(new_points[start_id], mid_point));
            }
            else if(finish_point_found)
            {
                special_edges.push_back(std::make_pair(new_points[finish_id], mid_point));
            }

        }
        //std::cerr << "size of connected points is " << connected_points.size() << std::endl;
        //std::cerr << graph.count(connected_points[0]) << std::endl;
        //std::cerr << graph.count(connected_points[1]) << std::endl;
        //std::cerr << graph.count(connected_points[2]) << std::endl;
        for(int k = 0; k < connected_points.size(); ++k)
            for(int r = 0; r < connected_points.size(); ++r)
                if( k != r )
                {
                    graph[connected_points[k]].push_back(connected_points[r]);
                    //graph[connected_points[r]].push_back(connected_points[k]);
                }
        for(auto edge: special_edges)
        {
            graph[edge.first].push_back(edge.second);
            graph[edge.second].push_back(edge.first);
        }
        std::cerr << "size of graph is" << graph.size() << std::endl;

    }

    return graph;
}

std::vector<Ogre::Vector2> BasicApp::analyse_points(Ogre::Vector3 start_pos, double min_dist)
{
    std::vector <Ogre::Vector2> points;
    std::vector <Ogre::Vector2> holes;

    points.push_back(Ogre::Vector2(-700, -700));
    points.push_back(Ogre::Vector2(start_pos.x, start_pos.z));

    points.push_back(Ogre::Vector2(-750, -750));
    points.push_back(Ogre::Vector2(750, -750));
    points.push_back(Ogre::Vector2(750, 750));
    points.push_back(Ogre::Vector2(-750, 750));

    for(LogicBody* ptr: parts)
    {
        Ogre::Entity* ent = ptr->ent;
        Ogre::AxisAlignedBox box = ent->getWorldBoundingBox(true);
        Ogre::Vector3 maxp = box.getMaximum();
        Ogre::Vector3 minp = box.getMinimum();
        Ogre::Vector3 ctr = box.getCenter();
        points.push_back(Ogre::Vector2(minp.x, minp.z));
        points.push_back(Ogre::Vector2(minp.x, maxp.z));
        points.push_back(Ogre::Vector2(maxp.x, maxp.z));
        points.push_back(Ogre::Vector2(maxp.x, minp.z));
        holes.push_back(Ogre::Vector2(ctr.x,  ctr.z));
    }
    std::map<pdd, std::vector< pdd > > graph = convert_points_to_graph(points, holes, min_dist);
    return get_shortest_path(graph);
}

struct deik_struct
{
    double dist;
    pdd prev;
};


std::vector<Ogre::Vector2> BasicApp::get_shortest_path(std::map<std::pair<double, double>, std::vector<std::pair<double, double> > >graph)
{
    std::cerr << "Ammount od points in graph is " << graph.size() <<std::endl;
    std::map<pdd, deik_struct> arr;
    std::map<pdd, bool> was;
    for(auto it = graph.begin(); it != graph.end(); ++it)
    {
        arr[it->first].dist = 1e6;
        arr[it->first].prev = it->first;
        was[it->first] = false;
    }
    arr[start_point].dist = 0;
    arr[start_point].prev = start_point;

    pdd cur_point = start_point;
    pdd prev_point;

    while(prev_point != cur_point)
    {
        was[cur_point] = true;
        for(pdd& point: graph[cur_point])
            if(!was[point])
            {
                double ndist = sqrt(sqr(point.first - cur_point.first) + sqr(point.second-cur_point.second));
                if(arr[cur_point].dist + ndist < arr[point].dist)
                {
                    arr[point].dist = arr[cur_point].dist + ndist;
                    arr[point].prev = cur_point;
                }
            }
        double buf= 1e5;
        prev_point = cur_point;
        for(auto it = graph.begin(); it != graph.end(); ++it)
            if(!was[it->first] && arr[it->first].dist <= buf)
            {
                buf = arr[it->first].dist;
                cur_point = it->first;
            }

    }
    /*
    arr = get_shortest_path(graph, points[start_id])
    cur_point = arr[points[finish_id]]["prev"]
    prev_id = finish_id
    while arr[cur_point]["prev"] != cur_point:
        cur_id = len(points)
        points.append(cur_point)
        edges.append((prev_id, cur_id))
        prev_id = cur_id
        cur_point = arr[cur_point]["prev"]
    */
    std::vector<Ogre::Vector2> path;
    path.emplace_back(finish_point.first, finish_point.second);
    cur_point = arr[finish_point].prev;
    while( arr[cur_point].prev != cur_point)
    {
        path.emplace_back(cur_point.first, cur_point.second);
        cur_point = arr[cur_point].prev;
    }
    std::cerr <<"path point ammount is " << path.size() <<std::endl;
    return path;
}

void BasicApp::save_points(std::__cxx11::string file_name)
{
    std::ofstream file = std::ofstream(file_name+".points");
    std::vector <Ogre::Vector2> points;
    std::vector <Ogre::Vector2> holes;

    for(LogicBody* ptr: parts)
    {
        Ogre::Entity* ent = ptr->ent;
        Ogre::AxisAlignedBox box = ent->getWorldBoundingBox(true);
        Ogre::Vector3 maxp = box.getMaximum();
        Ogre::Vector3 minp = box.getMinimum();
        Ogre::Vector3 ctr = box.getCenter();
        points.push_back(Ogre::Vector2(minp.x, minp.z));
        points.push_back(Ogre::Vector2(minp.x, maxp.z));
        points.push_back(Ogre::Vector2(maxp.x, maxp.z));
        points.push_back(Ogre::Vector2(maxp.x, minp.z));
        holes.push_back(Ogre::Vector2(ctr.x,  ctr.z));
    }

    file << points.size() << "\n";
    for(Ogre::Vector2& point: points)
    {
        file << point.x << " " << point.y << "\n";
    }

    file << holes.size() << "\n";
    for(Ogre::Vector2& point: holes)
    {
        file << point.x << " " << point.y << "\n";
    }
    file.close();
}

void BasicApp::save_scene(std::string filename)
{
    std::cerr << "saving scene\n";
    save_points(filename);
    using namespace rapidxml;
    xml_document<> doc;
    xml_node<>* scene = doc.allocate_node(node_element, "scene");
    xml_node<>* nodes = doc.allocate_node(node_element, "nodes");

    scene->append_attribute(doc.allocate_attribute("formatVersion", ""));
    doc.append_node(scene);
    scene->append_node(nodes);

    for(LogicBody* p: parts)
    {
        xml_node<>* part_node = doc.allocate_node(node_element, "node");
        part_node->append_attribute(doc.allocate_attribute("name", p->node->getName().c_str()));

        xml_node<>* position_node = doc.allocate_node(node_element, "position");
        position_node->append_attribute(doc.allocate_attribute("x", doc.allocate_string(std::to_string(p->pos.x).c_str())));
        position_node->append_attribute(doc.allocate_attribute("y", doc.allocate_string(std::to_string(p->pos.y).c_str())));
        position_node->append_attribute(doc.allocate_attribute("z", doc.allocate_string(std::to_string(p->pos.z).c_str())));

        xml_node<>* scale_node = doc.allocate_node(node_element, "scale");
        scale_node->append_attribute(doc.allocate_attribute("x", doc.allocate_string(std::to_string(p->scale).c_str())));
        scale_node->append_attribute(doc.allocate_attribute("y", doc.allocate_string(std::to_string(p->scale).c_str())));
        scale_node->append_attribute(doc.allocate_attribute("z", doc.allocate_string(std::to_string(p->scale).c_str())));

        xml_node<>* rotation_node = doc.allocate_node(node_element, "rotation");
        rotation_node->append_attribute(doc.allocate_attribute("qy", doc.allocate_string(std::to_string(p->rotation_yaw).c_str())));


        std::string meshName = p->ent->getMesh()->getName();
        xml_node<>* entity_node = doc.allocate_node(node_element, "entity");
        entity_node->append_attribute(doc.allocate_attribute("meshFile", doc.allocate_string(meshName.c_str())));

        part_node->append_node(position_node);
        part_node->append_node(scale_node);
        part_node->append_node(rotation_node);
        part_node->append_node(entity_node);
        nodes->append_node(part_node);
    }

    std::ofstream file_stored(filename+".scene");
    file_stored << doc;
    file_stored.close();
    doc.clear();
}


ScrollController::ScrollController(BasicApp* app):step(10), scene(app), openingMode(false), savingMode(false)
{

    m_ConsoleWindow = CEGUI::WindowManager::getSingletonPtr()->loadLayoutFromFile("part_config.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow( m_ConsoleWindow);
    m_ScaleSlider = static_cast<CEGUI::Slider*>(m_ConsoleWindow->getChild("ScaleSlider"));
    m_RotateSlider = static_cast<CEGUI::Slider*>(m_ConsoleWindow->getChild("RotateSlider"));
    m_MeshSelector = static_cast<CEGUI::Combobox*>(m_ConsoleWindow->getChild("SelectMeshBox"));
    m_AddButton = static_cast<CEGUI::PushButton*>(m_ConsoleWindow->getChild("AddButton"));
    m_OpenButton = static_cast<CEGUI::PushButton*>(m_ConsoleWindow->getChild("OpenButton"));
    m_SaveButton = static_cast<CEGUI::PushButton*>(m_ConsoleWindow->getChild("SaveButton"));
    m_FileEditbox = static_cast<CEGUI::Editbox*>(m_ConsoleWindow->getChild("FileEditbox"));


    add_item("robot");
    add_item("ninja");
    add_item("ogrehead");
    add_item("fish");
    add_item("razor");
    add_item("column");
    add_item("knot");
    //add_item("facial");
    //add_item("sibenik");
    //add_item("spine");



    CEGUI::ImageManager::getSingleton().addFromImageFile("default", "../media/images/ogre.png");
    CEGUI::ImageManager::getSingleton().addFromImageFile("meshes/robot", "../media/images/robot.png");
    CEGUI::ImageManager::getSingleton().addFromImageFile("meshes/ninja", "../media/images/ninja.jpg");
    CEGUI::ImageManager::getSingleton().addFromImageFile("meshes/ogrehead", "../media/images/ogrehead.png");
    m_ConsoleWindow->getChild("SampleImage")->setProperty("Image", "default");


    m_ScaleSlider->subscribeEvent(CEGUI::Slider::EventValueChanged,
                            CEGUI::Event::Subscriber(
                            &ScrollController::handle_scale_value_changed, this));
    m_RotateSlider->subscribeEvent(CEGUI::Slider::EventValueChanged,
                            CEGUI::Event::Subscriber(
                            &ScrollController::handle_rotate_value_changed, this));

    m_ConsoleWindow->getChild("x_less")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_less_x, this));
    m_ConsoleWindow->getChild("x_more")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_more_x, this));

    m_ConsoleWindow->getChild("y_less")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_less_y, this));
    m_ConsoleWindow->getChild("y_more")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_more_y, this));

    m_ConsoleWindow->getChild("z_less")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_less_z, this));
    m_ConsoleWindow->getChild("z_more")->subscribeEvent(
                CEGUI::PushButton::EventClicked,
                CEGUI::Event::Subscriber(&ScrollController::handle_more_z, this));

    m_ConsoleWindow->getChild("SelectMeshBox")->subscribeEvent(
                CEGUI::Combobox::EventDropListRemoved,
                CEGUI::Event::Subscriber(&ScrollController::handle_combobox_changed, this));

    m_AddButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                CEGUI::Event::Subscriber(&ScrollController::handle_add_entity, this));
    m_OpenButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                 CEGUI::Event::Subscriber(&ScrollController::handle_open_clicked, this));
    m_SaveButton->subscribeEvent(CEGUI::PushButton::EventClicked,
                                 CEGUI::Event::Subscriber(&ScrollController::handle_save_clicked, this));
}

void ScrollController::add_item(std::string mesh_name)
{
    CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(mesh_name.c_str());
    item->setSelectionBrushImage("TaharezLook/MultiListSelectionBrush");
    m_MeshSelector->addItem( item );
}


void ScrollController::setPart(LogicBody* part)
{
    if(scene->parts.empty())
        return;
    m_part = part;
    m_ScaleSlider->setCurrentValue(part->scale);
    m_RotateSlider->setCurrentValue(part->rotation_yaw + 180);

}

void ScrollController::handle_scale_value_changed(const CEGUI::EventArgs &e)
{
    if(scene->parts.empty())
        return;

    float val = std::max(m_ScaleSlider->getCurrentValue(), float(0.01));
    std::cout<<"Scale value Changed to " << val <<"\n";
    m_part->scale = val;
    m_part->isDirty = true;

}

void ScrollController::handle_rotate_value_changed(const CEGUI::EventArgs &e)
{
    if(scene->parts.empty())
        return;

    float val = m_RotateSlider->getCurrentValue() - 180.;
    std::cout<<"Rotate value Changed to " << val <<"\n";
    m_part->rotation_yaw = val;
    m_part->isDirty = true;
}

void ScrollController::handle_more_x(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.x += step;
    m_part->isDirty = true;
}

void ScrollController::handle_less_x(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.x -= step;
    m_part->isDirty = true;
}

void ScrollController::handle_more_y(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.y += step;
    m_part->isDirty = true;
}

void ScrollController::handle_less_y(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.y -= step;
    m_part->isDirty = true;
}

void ScrollController::handle_more_z(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.z += step;
    m_part->isDirty = true;
}

void ScrollController::handle_less_z(const CEGUI::EventArgs &ev)
{
    if(scene->parts.empty())
        return;

    m_part->pos.z -= step;
    m_part->isDirty = true;
}

void ScrollController::handle_combobox_changed(const CEGUI::EventArgs &ev)
{
    std::cerr << "Combobox value changed\n";
    CEGUI::String text = m_MeshSelector->getText();
    text = "meshes/" + text;
    m_ConsoleWindow->getChild("SampleImage")->setProperty("Image", text.c_str());
    m_AddButton->enable();
}

void ScrollController::handle_add_entity(const CEGUI::EventArgs &ev)
{
    std::cerr << "adding button pressed\n";
    CEGUI::String text = m_MeshSelector->getText();
    text += ".mesh";
    std::cerr << "pushing name\n";
    added_parts.push(text.c_str());
}

void ScrollController::handle_open_clicked(const CEGUI::EventArgs &e)
{
    filename = m_FileEditbox->getText().c_str();
    openingMode= true;
    m_part->isDirty = true;
}

void ScrollController::handle_save_clicked(const CEGUI::EventArgs &e)
{
    if(scene->parts.empty())
        return;

    filename = m_FileEditbox->getText().c_str();
    savingMode= true;

}



MovingObject::MovingObject(Ogre::SceneNode* nod, Ogre::Entity* en, Ogre::Vector3 position, Ogre::ManualObject* route_blank): LogicBody(nod, en), speed(100), cur_point_id(0), distance_to(0), route(route_blank)
{
    animation_state = en->getAnimationState("Walk");
    animation_state->setLoop(true);
    animation_state->setEnabled(true);
    pos = position;
    points.push_back(pos);
    route->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);
        route->position(points[0]);
        route->position(points[0].x, points[0].y, points[0].z+10);
        route->position(points[0]);
    route->end();
}

void MovingObject::add_point(Ogre::Vector3 pos)
{
    points.push_back(pos);

    route->beginUpdate(0);
    for(int i = 1; i < points.size(); ++i)
    {
        route->position(points[i]);
        route->position(points[i].x, points[i].y+10, points[i].z);
    }
    route->end();
}


void MovingObject::chanage_state(const Ogre::FrameEvent& fe)
{
    //std::cerr << "MovingObject Function";

    node->setScale(scale, scale, scale);
    node->setPosition(pos);

    float move = speed * fe.timeSinceLastFrame;
    distance_to -= move;

    if(distance_to <= 0)
    {
        pos = points[cur_point_id];

        cur_point_id = (cur_point_id + 1) % points.size();
        direction = points[cur_point_id] - pos;
        distance_to = direction.normalise();

        Ogre::Vector3 src = node->getOrientation() * Ogre::Vector3::UNIT_X;

        if ((1.0 + src.dotProduct(direction)) < 0.0001)
        {
            node->yaw(Ogre::Degree(180));
        }
        else
        {
            Ogre::Quaternion quat = src.getRotationTo(direction);
            node->rotate(quat);
        }
        rotation_yaw = src.angleBetween(Ogre::Vector3::UNIT_X).valueDegrees();
    }
    else
    {
        pos += move * direction;
    }

    animation_state->addTime(fe.timeSinceLastFrame);
    isDirty = false;
}

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  #define WIN32_LEAN_AND_MEAN
  #include "windows.h"
#endif

#ifdef __cplusplus
  extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
  INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
  int main(int argc, char *argv[])
#endif
{
  BasicApp app;

  try
  {
    app.go();
  }
  catch(Ogre::Exception& e)
  {
    #if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
      MessageBox(
    NULL,
    e.getFullDescription().c_str(),
    "An exception has occured!",
    MB_OK | MB_ICONERROR | MB_TASKMODAL);
    #else
      std::cerr << "An exception has occured: " <<
    e.getFullDescription().c_str() << std::endl;
    #endif
  }

  return 0;
}

#ifdef __cplusplus
  }
#endif
