#ifndef BASICAPP_H
#define BASICAPP_H

#include <OgreRoot.h>
#include <OgreCamera.h>
#include <OgreViewport.h>
#include <OgreParticleSystem.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreException.h>
#include <OgreEntity.h>
#include <OgreFrameListener.h>
#include <OgreLogManager.h>
#include <OgreTextureManager.h>
#include <OgreMeshManager.h>
#include <OgreMesh.h>
#include <OgreManualObject.h>
#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include <OgreWindowEventUtilities.h>
#include <OgreAnimationState.h>


#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <CEGUI/CEGUI.h>

#include <CEGUI/WindowManager.h>

#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include <CEGUI/EventArgs.h>


#include <SdkCameraMan.h>

#include <vector>
#include <iostream>
#include <queue>
#include <fstream>
#include <utility>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "rapidxml/rapidxml_utils.hpp"

//#define REAL double
#include "tri_src/triangle_impl.h"

#define pdd std::pair<double, double>
#define sqr(a) ((a)*(a))

class LogicBody
{
public:
    LogicBody(Ogre::SceneNode* nod, Ogre::Entity* en):
        node(nod),
        ent(en),
        scale(1),
        rotation_yaw(0),
        pos(Ogre::Vector3::ZERO),
        isDirty(false)
    {}
    Ogre::SceneNode* node;
    Ogre::Entity* ent;
    float scale;
    int rotation_yaw;
    Ogre::Vector3 pos;
    bool isDirty;
    virtual void chanage_state(const Ogre::FrameEvent& fe)
    {
        //std::cerr << "LogicBody Function";

        if(isDirty)
        {
            node->setScale(scale, scale, scale);
            node->setOrientation(Ogre::Quaternion(Ogre::Degree(rotation_yaw),Ogre::Vector3::UNIT_Y));
            node->setPosition(pos);
            isDirty = false;
        }
    }
};



class MovingObject: public LogicBody
{
public:

    std::vector<Ogre::Vector3> points;
    Ogre::Vector3 direction;

    float speed;
    float distance_to;
    int cur_point_id;

    Ogre::AnimationState* animation_state;
    Ogre::ManualObject* route;

    MovingObject(Ogre::SceneNode* nod, Ogre::Entity* en, Ogre::Vector3 position, Ogre::ManualObject* route_blank);
    void add_point(Ogre::Vector3 pos);
    void chanage_state(const Ogre::FrameEvent& fe) override;
};

class ScrollController;

class BasicApp
  : public Ogre::WindowEventListener,
    public Ogre::FrameListener,
    public OIS::KeyListener,
    public OIS::MouseListener
{
public:
  BasicApp();
  ~BasicApp();

    std::vector <LogicBody*> parts;
    void go();
    void add_entity(std::string mesh_name);

private:

    virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

    virtual bool keyPressed(const OIS::KeyEvent& ke);
    virtual bool keyReleased(const OIS::KeyEvent& ke);

    virtual bool mouseMoved(const OIS::MouseEvent& me);
    virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

    virtual void windowResized(Ogre::RenderWindow* rw);
    virtual void windowClosed(Ogre::RenderWindow* rw);

    bool has_intersections(Ogre::Entity*);

    bool setup();
    bool configure();
    void chooseSceneManager();
    void createCamera();
    void createScene();
    void destroyScene();
    void createFrameListener();
    void createViewports();
    void setupResources();
    void createResourceListener();
    void loadResources();
    void save_points(std::__cxx11::string file_name);

    bool mShutdown;

    Ogre::Root* mRoot;
    Ogre::Camera* mCamera;
    Ogre::SceneNode* mCameraNode;

    Ogre::SceneManager* mSceneMgr;
    Ogre::RenderWindow* mWindow;
    Ogre::String mResourcesCfg;
    Ogre::String mPluginsCfg;

    OgreBites::SdkCameraMan* mCameraMan;

    // CEGUI
    bool setupCEGUI();

    CEGUI::OgreRenderer* mRenderer;

    // OIS
    OIS::Mouse* mMouse;
    OIS::Keyboard* mKeyboard;
    OIS::InputManager* mInputMgr;

  //////////////////////
  // Tutorial Section //
  //////////////////////

    void save_scene(std::__cxx11::string filename);
    void load_scene(std::__cxx11::string file_name);
    std::vector<Ogre::Vector2> analyse_points(Ogre::Vector3 start_pos, double min_dist);
    std::map<std::pair<double, double>, std::vector<std::pair<double, double> > >convert_points_to_graph(std::vector<Ogre::Vector2> const& points, std::vector<Ogre::Vector2> const& holes, double min_dist);
    std::vector<Ogre::Vector2> get_shortest_path(std::map<std::pair<double, double>, std::vector<std::pair<double, double> > > graph);

    int start_id;
    int finish_id;
    pdd start_point;
    pdd finish_point;


    bool addingMode;
    bool mLMouseDown, mRMouseDown;
    bool mMovableFound;
    bool movingMode;
    bool rotationMode;
    bool scalingMode;
    double scalePrevDist;
    double scalePrevVal;
    ScrollController* myGUI;

    Ogre::Plane plane;
    Ogre::RaySceneQuery* mRayScnQuery;
    Ogre::SceneNode* CursorNode;
    Ogre::Entity* CursorEnt;
    Ogre::SceneNode* main_nod;
    std::vector<LogicBody*>::iterator cur_part_it;

};

class ScrollController
{
public:

    bool openingMode;
    bool savingMode;
    std::queue <std::string> added_parts;
    std::string filename;
    ScrollController(BasicApp* app);
    void add_item(std::string mesh_name);
    void setPart(LogicBody* part);

private:
    BasicApp* scene;
    LogicBody* m_part;
    float step;

    //CEGUI::StaticImage* m_image;
    CEGUI::Window* m_ConsoleWindow;
    CEGUI::Slider* m_ScaleSlider;
    CEGUI::Slider* m_RotateSlider;
    CEGUI::Combobox* m_MeshSelector;
    CEGUI::Editbox* m_FileEditbox;
    CEGUI::PushButton* m_AddButton;
    CEGUI::PushButton* m_OpenButton;
    CEGUI::PushButton* m_SaveButton;



    void handle_scale_value_changed(const CEGUI::EventArgs &e);

    void handle_rotate_value_changed(const CEGUI::EventArgs &e);

    void handle_more_x(const CEGUI::EventArgs &ev);

    void handle_less_x(const CEGUI::EventArgs &ev);

    void handle_more_y(const CEGUI::EventArgs &ev);

    void handle_less_y(const CEGUI::EventArgs &ev);

    void handle_more_z(const CEGUI::EventArgs &ev);

    void handle_less_z(const CEGUI::EventArgs &ev);

    void handle_combobox_changed(const CEGUI::EventArgs &ev);

    void handle_add_entity(const CEGUI::EventArgs &ev);

    void handle_open_clicked(const CEGUI::EventArgs &e);

    void handle_save_clicked(const CEGUI::EventArgs &e);

};


#endif
